# -*- coding: UTF-8 -*-
# This file is part of Tryton.  The COPYRIGHT file at the top level
# of this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard
from trytond.pool import Pool


class TicketSynchronizationLog(ModelView, ModelSQL):
    "Ticket Synchronization Log"
    _name = "ticket.synchronization.log"
    _description = __doc__
    _rec_name = 'timestamp'

    timestamp = fields.DateTime('Timestamp', required=True, readonly=True,
            select=1)
    user = fields.Many2One('res.user', 'User', required=True, readonly=True,
            select=1)
    tracker = fields.Many2One('ticket.tracker', 'Tracker', required=True,
            readonly=True)
    lines = fields.One2Many('ticket.synchronization.log.line', 'log',
            'Synchronization Log Lines', readonly=True)

    def __init__(self):
        super(TicketSynchronizationLog, self).__init__()
        self._order.insert(0, ('timestamp', 'DESC'))

TicketSynchronizationLog()


class TicketSynchronizationLogLine(ModelView, ModelSQL):
    "Ticket Synchronization Log Line"
    _name = "ticket.synchronization.log.line"
    _description = __doc__
    _rec_name = 'message'

    log = fields.Many2One('ticket.synchronization.log', 'Synchronization Log',
            required=True, readonly=True, ondelete='CASCADE', select=1)
    ticket = fields.Many2One('ticket.work', 'Ticket', readonly=True, select=1,
            ondelete='CASCADE')
    state = fields.Selection([
                ('ok', 'Ok'),
                ('syncing', 'Syncing...'),
                ('not_ok', 'Not ok'),
            ], 'State', required=True, readonly=True)
    message = fields.Text('Message', readonly=True)

    def __init__(self):
        super(TicketSynchronizationLogLine, self).__init__()
        self._order.insert(0, ('log', 'DESC'))

TicketSynchronizationLogLine()


class SynchronizeTracker(Wizard):
    'Synchronize Tracker'
    _name = 'ticket.synchronize'
    states = {
        'init': {
            'result': {
                'type': 'action',
                'action': '_synchronize',
                'state': 'end',
            },
        },
    }

    def __init__(self):
        super(SynchronizeTracker, self).__init__()
        self._rpc['_synchronize'] = True

    def _synchronize(self, data):
        pool = Pool()
        model_data_obj = pool.get('ir.model.data')
        act_window_obj = pool.get('ir.action.act_window')
        tracker_obj = pool.get('ticket.tracker')

        tracker_obj.synchronize()

        act_window_id = model_data_obj.get_id('ticket_tracker',
                'act_sync_log_form')
        res = act_window_obj.read(act_window_id)

        return res

SynchronizeTracker()


class SynchronizeTicket(Wizard):
    'Synchronize Ticket'
    _name = 'ticket.synchronize_ticket'
    states = {
        'init': {
            'result': {
                'type': 'action',
                'action': '_synchronize',
                'state': 'end',
            }
        }
    }

    def _synchronize(self, data):
        pool = Pool()
        tracker_obj = pool.get('ticket.tracker')
        tracker_obj.synchronize(tickets=data['ids'])
        return {}

SynchronizeTicket()


class SynchronizeTicketByTrackerCodeInit(ModelView):
    'Synchronize Ticket By Tracker Code Init'
    _name = 'ticket.synchronize_ticket_by_tracker_code.init'
    _description = __doc__

    tracker = fields.Many2One('ticket.tracker', 'Tracker', required=True,
        domain=[('active', '=', True)])
    tracker_code = fields.Char('Tracker Code', required=True)

SynchronizeTicketByTrackerCodeInit()


class SynchronizeTicketByTrackerCode(Wizard):
    'Synchronize Ticket By Tracker Code'
    _name = 'ticket.synchronize_ticket_by_tracker_code'
    states = {
        'init': {
            'result': {
                'type': 'form',
                'object': 'ticket.synchronize_ticket_by_tracker_code.init',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('sync', 'Synchronize', 'tryton-ok'),
                ],

            }
        },
        'sync': {
            'result': {
                'type': 'action',
                'action': '_synchronize',
                'state': 'end',
            }
        }
    }

    def _synchronize(self, data):
        tracker_obj = Pool().get('ticket.tracker')
        tracker = tracker_obj.browse(data['form']['tracker'])
        tracker_tickets = tracker_obj._get_tickets_from_tracker(tracker,
            [data['form']['tracker_code']])
        tracker_obj._sync_tickets(tracker, tracker_tickets)
        return {}

SynchronizeTicketByTrackerCode()
