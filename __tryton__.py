# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Ticket Tracker',
    'name_de_DE': 'Ticket Ticketsystem-Anbindung',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Provides the basic functionalities for the synchronization
      of tickets with an external ticket/issue tracking system.
    - The functionality for specific issue tracking systems is provided
      by additional modules in the namespace trytond_ticket_tracker_*
    ''',
    'description_de_DE': '''
    - Stellt die Basisfunktionalitäten für die Synchronisation von Tickets
      mit einem externen Ticketsystem (Issue-Tracking-System) zur Verfügung.
    - Die Funktionalität spezifischer Ticketsysteme wird mittels weiterer
      Module zur Verfügung gestellt, die sich im Namensraum
      trytond_ticket_tracker_* befinden.
    ''',
    'depends': [
        'ticket',
    ],
    'xml': [
        'synchronization.xml',
        'tracker.xml',
        'ticket.xml',
        'configuration.xml',
        'cron.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
