# -*- coding: UTF-8 -*-
# This file is part of Tryton.  The COPYRIGHT file at the top level
# of this repository contains the full copyright notices and license terms.
from datetime import datetime
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.modules.ticket_tracker import TrackerHandler
from trytond.pool import Pool


class Ticket(ModelSQL, ModelView):
    _name = 'ticket.work'

    tracker_origin = fields.Many2One('ticket.tracker', 'Tracker Origin',
        readonly=True)
    tracker_code = fields.Char('Tracker Code', readonly=True)
    tracker_backlink = fields.Function(fields.Char('Tracker Backlink'),
        'get_backlink')
    tracker_last_change = fields.DateTime('Tracker Last Change',
        readonly=True)
    synchronization_log_lines = fields.One2Many(
            'ticket.synchronization.log.line', 'ticket',
            'Synchronization Log', domain=[
                ('ticket', '=', Eval('active_id')),
            ], readonly=True, depends=['active_id'])
    last_sync_state = fields.Function(fields.Selection([
            ('ok', 'Ok'),
            ('syncing', 'Syncing...'),
            ('not_ok', 'Not ok'),
        ], 'Last sync state', readonly=True), 'get_last_sync_state',
        searcher='search_last_sync_state')

    def __init__(self):
        super(Ticket, self).__init__()
        self._error_messages.update({
            'delete_billing':
                'The deletion of tickets with billing lines in '
                'state "confirmed" or "invoiced" is not allowed!',
        })

    def get_backlink(self, ids, name):
        tracker_obj = Pool().get('ticket.tracker')

        if isinstance(ids, (int, long)):
            ids = [ids]

        tracker_handlers = {}
        tracker_ids = tracker_obj.search([])
        for tracker in tracker_obj.browse(tracker_ids):
            if tracker.type and tracker.type not in tracker_handlers:
                tracker_handlers[tracker.type] = TrackerHandler.get_handler(
                    tracker)

        res = {}
        for ticket in self.browse(ids):
            res[ticket.id] = tracker_handlers[
                ticket.tracker_origin.type].get_backlink(
                ticket.tracker_code)
        return res

    def get_last_sync_state(self, ids, name):
        sync_obj = Pool().get('ticket.synchronization.log.line')
        res = {}
        for id_ in ids:
            sync_state = sync_obj.search_read([('ticket', '=', id_)], limit=1,
                order=[('id', 'DESC')], fields_names=['state'])
            res[id_] = sync_state['state']
        return res

    def search_last_sync_state(self, name, clause):
        sync_obj = Pool().get('ticket.synchronization.log.line')

        ids = []
        for id_ in self.search([]):
            sync_value = sync_obj.search_read([
                    ('ticket', '=', id_),
                ], fields_names=['ticket', 'state'], limit=1,
                order=[('id', 'DESC')])
            if sync_value.get('state') == clause[2]:
                ids.append(sync_value.get('ticket'))
        return [('id', 'in', ids)]

    def delete(self, ids):
        sync_log_obj = Pool().get('ticket.synchronization.log')
        sync_log_line_obj = Pool().get('ticket.synchronization.log.line')

        if isinstance(ids, (int, long)):
            ids = [ids]

        sync_log_line_ids = sync_log_line_obj.search([('ticket', 'in', ids)])
        sync_log_ids = sync_log_obj.search([])

        for ticket in self.browse(ids):
            for line in ticket.timesheet_lines:
                for billing in line.billing_lines:
                    if billing.state in ('confirmed', 'invoice'):
                        self.raise_user_error('delete_billing')

        res = super(Ticket, self).delete(ids)
        sync_log_line_obj.delete(sync_log_line_ids)

        for log in sync_log_obj.browse(sync_log_ids):
            if not log.lines:
                sync_log_obj.delete(log.id)

        return res

Ticket()

