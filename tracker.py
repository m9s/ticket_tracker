# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import logging
import datetime
from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction
from trytond.pool import Pool


class TicketTracker(ModelSQL, ModelView):
    "Ticket Tracker"
    _name = "ticket.tracker"
    _description = __doc__

    name = fields.Char('Name', required=True)
    sequence = fields.Integer('Sequence')
    type = fields.Selection([], 'Type', required=True)
    active = fields.Boolean('Active', help='When checked, '
        'Tracker will be synchronized with Tryton tickets.')
    url = fields.Char('URL', help='The website of the Tracker '
        'to connect.')
    login = fields.Char('Login', help='The username for the '
        'Tracker connection.')
    password = fields.Char('Password', help='The password for the '
        'RTracker connection.'
        '\n\nWARNING: The password is stored as clear-text in database!')
    company = fields.Many2One('company.company', 'company', required=True)
    configuration = fields.Many2One('ticket.configuration', 'Configuration')

    def __init__(self):
        super(TicketTracker, self).__init__()
        self._error_messages.update({
                'tracker_not_active': 'You try to sync a ticket of the '
                    'inactive tracker %s.',
                'no_connection_tracker': 'Could not connect to tracker %s',
            })

    def default_active(self):
        return True

    def default_company(self):
        return Transaction().context.get('company') or False

    def synchronize(self, tickets=None):
        pool = Pool()
        config_obj =  pool.get('ticket.configuration')
        ticket_obj = pool.get('ticket.work')
        logger = logging.getLogger('ticket.tracker')

        logger.info('Start Synchronization ' + str(datetime.datetime.now()))

        config = config_obj.browse(config_obj.get_singleton_id())

        trackers = config.trackers
        ticketsbytracker = {}
        if tickets:
            if isinstance(tickets[0], (int, long)):
                tickets = ticket_obj.browse(tickets)

            id2tracker = {}
            for tracker in config.trackers:
                id2tracker[tracker.id] = tracker
                ticketsbytracker.setdefault(tracker.id, [])
            for ticket in tickets:
                ticketsbytracker[ticket.tracker_origin.id].append(
                    ticket.tracker_code)
            trackers = [id2tracker[x] for x in ticketsbytracker.keys()
                if ticketsbytracker[x]]

        for tracker in trackers:
            if not tracker.active:
                continue
            ticket_ids = ticketsbytracker.get(tracker.id) or None
            tracker_tickets = self._get_tickets_from_tracker(tracker,
                ticket_ids=ticket_ids)
            if tracker_tickets:
                self._sync_tickets(tracker, tracker_tickets)
            else:
                self.raise_user_error('no_connection_tracker',
                    error_args=(tracker.name,))

        logger.info('Stop Synchronization ' + str(datetime.datetime.now()))

    def _get_tickets_from_tracker(self, tracker, ticket_ids=None):
        res = []
        if not tracker.active:
            self.raise_user_error('tracker_not_active',
                error_args=(tracker.name,))
        tracker_handler = TrackerHandler.get_handler(tracker)
        if tracker_handler.check_connection():
            res = tracker_handler.get_tickets(ticket_ids=ticket_ids)
        return res

    def _sync_tickets(self, tracker, tracker_tickets):
        pool = Pool()
        log_obj = pool.get('ticket.synchronization.log')
        log_line_obj = pool.get('ticket.synchronization.log.line')

        with Transaction().set_context(company=tracker.company.id):
            log_id = log_obj.create({
                'timestamp': datetime.datetime.now(),
                'user': Transaction().user,
                'tracker': tracker['id']})
            for tracker_ticket in tracker_tickets:
                if not self._check_update(tracker_ticket,
                        tracker.type):
                    continue
                tryton_ticket_id, tryton_ticket_data, message, \
                    sync_state = \
                        self.map_tryton_ticket(
                            tracker_ticket, tracker.type)
                ticket_id = \
                    self.create_or_update_tryton_ticket(
                        ticket_id=tryton_ticket_id,
                        data=tryton_ticket_data,
                        ticket=tracker_ticket, tracker=tracker)

                log_line_obj.create({
                    'log': log_id,
                    'ticket': ticket_id,
                    'state': sync_state,
                    'message': message,
                    })

    def _check_update(self, tracker_ticket, type):
        return None

    def map_tryton_ticket(self, data, type):
        return None

    def create_or_update_tryton_ticket(self, ticket_id=None, data=None,
            ticket=None, message='', tracker=None):
        ticket_obj = Pool().get('ticket.work')

        if not data:
            return
        if not ticket_id:
            data['tracker_origin'] = tracker.id
            data['company'] = tracker.company.id
            ticket_id = ticket_obj.create(data)
            return ticket_id
        if isinstance(ticket_id, list):
            ticket_id = ticket_id[0]
        ticket = ticket_obj.browse(ticket_id)
        for key in data.keys():
            data_value = data[key]
            # Check for update
            value = getattr(ticket, key)
            if value and hasattr(value, 'id'):
                value = value.id
            if isinstance(data_value, str):
                data_value = unicode(data_value, encoding='utf-8',
                    errors='replace')
            if data_value == value:
                # Do not update equal fields
                del data_value
        if not ticket.tracker_origin:
            data['tracker_origin'] = tracker.id
        if not ticket.company:
            data['company'] = tracker.company.id
        if data:
            ticket_obj.write(ticket_id, data)
        return ticket_id

TicketTracker()


class TrackerHandler(object):
    '''
    Inherit from this object to provide specific methods by handler
    '''
    @classmethod
    def get_handler(cls, tracker):
        for subclass in TrackerHandler.__subclasses__():
            if subclass._name == tracker.type:
                return subclass(tracker)
        raise Exception, ('Tracker Type "%s" not supported!' % (tracker.type,))

    def check_connection(self):
        raise NotImplementedError

    def get_tickets(self, ticket_ids=None):
        raise NotImplementedError

    def get_backlink(self, code):
        raise NotImplementedError
